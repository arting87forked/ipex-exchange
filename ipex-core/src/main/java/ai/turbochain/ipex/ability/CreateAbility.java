package ai.turbochain.ipex.ability;

/**
 * @author jack
 * @Title:
 * @Description:
 * @date 2020-03-27
 */
public interface CreateAbility<T> {
    //创建能力
    T transformation();
}
