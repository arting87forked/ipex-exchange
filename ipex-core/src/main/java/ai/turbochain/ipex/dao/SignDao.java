package ai.turbochain.ipex.dao;

import ai.turbochain.ipex.constant.SignStatus;
import ai.turbochain.ipex.dao.base.BaseDao;
import ai.turbochain.ipex.entity.Sign;

/**
 * @author jack
 * @Description:
 * @date 2020/5/311:10
 */
public interface SignDao extends BaseDao<Sign> {
    Sign findByStatus(SignStatus status);
}
