package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMemberLevelFee is a Querydsl query type for MemberLevelFee
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMemberLevelFee extends EntityPathBase<MemberLevelFee> {

    private static final long serialVersionUID = -1278110472L;

    public static final QMemberLevelFee memberLevelFee = new QMemberLevelFee("memberLevelFee");

    public final DateTimePath<java.util.Date> createTime = createDateTime("createTime", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> fee = createNumber("fee", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> memberLevelId = createNumber("memberLevelId", Long.class);

    public final StringPath symbol = createString("symbol");

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QMemberLevelFee(String variable) {
        super(MemberLevelFee.class, forVariable(variable));
    }

    public QMemberLevelFee(Path<? extends MemberLevelFee> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMemberLevelFee(PathMetadata metadata) {
        super(MemberLevelFee.class, metadata);
    }

}

